// 2. This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var players = [];

var mainPlayer;
function onYouTubeIframeAPIReady() {
    mainPlayer = new YT.Player('playerMain', {
        height: '500',
        width: '800',
        videoId: 'O7q811tgmRY',
        playerVars: {
            'playsinline': 1
        },
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });

    unixPlayer = new YT.Player('playerUnix', {
        height: '500',
        width: '800',
        videoId: 'fW14vqjUP9c',
        playerVars: {
            'playsinline': 1
        },
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });

    winPlayer = new YT.Player('playerWin', {
        height: '500',
        width: '800',
        videoId: 'LnaCH_uS7Gc',
        playerVars: {
            'playsinline': 1
        },
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });

    gitIntroPlayer = new YT.Player('playerGitIntro', {
        height: '500',
        width: '800',
        videoId: 'k-Oxr2NcBrY',
        playerVars: {
            'playsinline': 1
        },
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });


    gitTermPlayer = new YT.Player('playerGitTerm', {
        height: '500',
        width: '800',
        videoId: 'YJrCwJ_O7a0',
        playerVars: {
            'playsinline': 1
        },
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });

    gitAppPlayer = new YT.Player('playerGitApp', {
        height: '500',
        width: '800',
        videoId: 'W8frEPDxybY',
        playerVars: {
            'playsinline': 1
        },
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
}

// Function to change

// Id of headlines without Vid and return the player object
function getPlayer(id) {
    if (id.includes("main")) {
        return mainPlayer;
    } else if(id.includes("unix")) {
        return unixPlayer;
    } else if(id.includes("win")) {
        return winPlayer;
    } else if(id.includes("gitIntro")) {
        return gitIntroPlayer;
    } else if(id.includes("gitTerm")) {
        return gitTermPlayer;
    } else if(id.includes("gitApp")) {
        return gitAppPlayer;
    }
}

// player IFrome id return times index
function getContentTimes(playerID) {
    if (playerID.includes("Main")) {
        return times["mainPlayer"];
    } else if(playerID.includes("Unix")) {
        return times["unixPlayer"];
    } else if(playerID.includes("Win")) {
        return times["winPlayer"];
    } else if(playerID.includes("playerGitIntro")) {
        return times["gitIntroPlayer"];
    } else if(playerID.includes("playerGitTerm")) {
        return times["gitTermPlayer"];
    } else if(playerID.includes("playerGitApp")) {
        return times["gitAppPlayer"];
    }
}

// player IFrame IF outline container custom ID of H1
function getContentClass(playerID) {
    if (playerID.includes("Main")) {
        return '#outline-container-mainVid .outline-4';
    } else if(playerID.includes("Unix")) {
        return '#outline-container-unixVid .outline-4';
    } else if(playerID.includes("Win")) {
        return '#outline-container-winVid .outline-4';
    } else if(playerID.includes("GitIntro")) {
        return '#outline-container-gitIntroVid .outline-4';
    } else if(playerID.includes("playerGitTerm")) {
        return '#outline-container-gitTermVid .outline-4';
    } else if(playerID.includes("playerGitApp")) {
        return '#outline-container-gitAppVid .outline-4';
    }
}


// heading timers
// 
times = {
    //main
    "mainPlayer" : [[0, 53],
                    [54, 112],
                    [9, 10]],
    //unix
    "unixPlayer" : [[0, 39],
                    [40, 70],
                    [70, 116],
                    [116, 169],
                    [198, 257],
                    [257, 323],
                    [324, 479],
                    [562, 668],
                    [668, 769],
                    [769, 902],
                    [985, 1016],
                   [1039, 1244],],
    //win
    "winPlayer" : [[0, 42],
                   [42, 154],
                   [154, 179],
                   [180, 277],
                   [277, 343],
                   [343, 422],
                   [445, 536],
                   [536, 668],
                   [668, 753],
                   [753, 897],
                  [897, 936],],

    //Git intro
    "gitIntroPlayer" : [[0, 29],
                        [30, 60],
                        [61, 256],
                        [257, 361],
                        [362, 633],],

    //Git Term
    "gitTermPlayer" : [[0, 36],
                       [36, 100],
                       [100, 194],
                       [194, 377],
                       [377, 401],
                       [401, 491],
                       [491, 623],
                       [623, 655],
                       [655, 704],
                       [704, 720],
                       [720, 785],
                       [785, 820],
                       [820, 990],
                       [990, 1040],
                       [1040, 1070],
                       [1070, 1142],
                       [1142, 1183],
                       [1183, 1268],
                       [1268, 1397],
                      ],

    //Git App
    "gitAppPlayer" : [[0, 47],
                      [47, 70],
                      [70, 120],
                      [120, 232],
                      [232, 316],
                      [316, 390],
                      [390, 449],
                      [450, 489],
                      [489, 608],
                     ],
}


// Add all players to `players` (order is significant)
function onPlayerReady(event) {
    //event.target.playVideo();
    closeAll();
    startInterval(event.target);
    attacheSeekClick(event.target);
    tabSwitchEvent();
    players = [mainPlayer, unixPlayer, winPlayer, gitIntroPlayer, gitTermPlayer, gitAppPlayer];
}


/*
 *
 * ----------------- DON'T CHANGE ANYTHING AFTER THIS POINT-----------------
 *
 */






// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
var done = false;
function onPlayerStateChange(event) {
    
}


checkInt = []

function startInterval(player) {
    //checks every 100ms to see if the video has reached 6s
    checkInt[checkInt.length] = setInterval(function() {
        //if (player.getPlayerState() == YT.PlayerState.PLAYING) {
        let currTime = player.getCurrentTime();
        let cTime = getContentTimes(player.m.id);
            for (i in cTime) {
                if (cTime[i][0] <= currTime && cTime[i][1] > currTime) {
                    hsExpand($(getContentClass(player.m.id)).eq(i).children(':header'))
                }
                else {
                    hsCollapse($(getContentClass(player.m.id)).eq(i).children(':header'))
                }
            }
        //}
   }, 100)
}

function tabSwitchEvent() {
    for (let i = 0; i < $("ul#tabs li").length; i++) {
        $("ul#tabs li").eq(i).click(function(){
            for (let j = 0; j < players.length; j++) {
                if(i != j) {
                    players[j].pauseVideo();
                }
            }
        })
    }
}


function closeAll() {
    // Function to close all Tutorial Elements
  $('#content .outline-4').each(function() {
        hsCollapse($(this).children(':header'));
  });
}


function attacheSeekClick(player) {
    for (let i = 0; i < $(getContentClass(player.m.id)).length; i++) {
        $(getContentClass(player.m.id)).eq(i).click(function(){
            console.log(player.m.id)
            player.seekTo(getContentTimes(player.m.id)[i][0], true);
            }
        )
    }
}


